#lang racket

(require math)

(provide f-parse)

(define (f-parse fstr)
  (define (fchars->flst fchars)
  (define (char-numeric-ext? c)
    (or (char-numeric? c) (char=? c #\.)))

  (define (get-depth lst)
    (define len 0)
    
    (for-each (lambda (x)
                (if (list? x) (set! len (+ len (get-depth x)))
                    (set! len (+ len 1))))
              lst)

    len)
  
  (define cur-token '())
  (define token-is-n? #f)
    
  (define (make-flst fchars)
    (cond [(or (empty? fchars) (char=? (first fchars) #\)))
           (if (cons? cur-token)
               (if token-is-n? `(,(string->number (list->string (reverse cur-token))))
                     `(,(string->symbol (list->string (reverse cur-token)))))
                 '())]
            [(char=? (first fchars) #\()
             (define inner (fchars->flst (rest fchars)))
             (define rst (rest fchars))
             
             (for ([i (get-depth inner)])
               (set! rst (rest rst)))

             (displayln (rest rst))

             (cond [(cons? cur-token)
                    (define tk cur-token)
                    (set! cur-token '())

                    (if token-is-n? (cons (string->number (list->string (reverse tk))) (cons inner (make-flst (rest rst))))
                        (cons (string->symbol (list->string (reverse tk))) (cons inner (make-flst (rest rst)))))]
                   [else (cons inner (make-flst (rest rst)))])]
            [(char-numeric-ext? (first fchars))
             (set! cur-token (cons (first fchars) cur-token))
             (set! token-is-n? #t)
             
             (make-flst (rest fchars))]
            [(char=? (first fchars) #\x)
             (cond [(cons? cur-token)
                    (define tk cur-token)
                    (set! cur-token '())
                    
                    (if token-is-n? (cons (string->number (list->string (reverse tk))) (cons 'x (fchars->flst (rest fchars))))
                        (cons (string->symbol (list->string (reverse tk))) (cons 'x (fchars->flst (rest fchars)))))]
                   [else (cons 'x (fchars->flst (rest fchars)))])]
            [(char-alphabetic? (first fchars))
             (cond [(and token-is-n? (cons? cur-token))
                    (define tk cur-token)
                    (set! cur-token `(,(first fchars)))
                    (set! token-is-n? #f)]
                   [else (set! cur-token (cons (first fchars) cur-token))
                         (set! token-is-n? #f)])

             (make-flst (rest fchars))]
            [else (cond [(cons? cur-token)
                    (define tk cur-token)
                    (set! cur-token '())
                    
                    (if token-is-n? (cons (string->number (list->string (reverse tk))) (cons (first fchars) (fchars->flst (rest fchars))))
                        (cons (string->symbol (list->string (reverse tk))) (cons (first fchars) (fchars->flst (rest fchars)))))]
                   [else (cons (first fchars) (fchars->flst (rest fchars)))])]))

    (make-flst fchars))

  (define (eval-op-exp opc a b)
    (define fa (flst->f a))
    (define fb (flst->f b))
    
    (cond [(and (number? fa) (number? fb)) (opc fa fb)]
          [(number? fa) (lambda (x) (opc fa (fb x)))]
          [(number? fb) (lambda (x) (opc (fa x) fb))]
          [else (lambda (x) (opc (fa x) (fb x)))]))

  (define ops (hash 'sin sin
                    'cos cos
                    'tan tan
                    'abs abs))
  (define consts (hash 'e euler.0
                       'pi pi))
  
  (define (flst->f flst)
    (match flst
      [`(,a ..1 #\+ ,b ..1) (eval-op-exp + a b)]
      [`(,a ..1 #\- ,b ..1) (eval-op-exp - a b)]
      [`(,a ..1 #\* ,b ..1) (eval-op-exp * a b)]
      [`(,a ..1 #\/ ,b ..1) (eval-op-exp / a b)]
      [`(,a ..1 #\^ ,b ..1) (eval-op-exp expt a b)]
      [`(#\- ,a ..1)
       (define fa (flst->f a))
 
       (if (number? fa) (- fa)
           (lambda (x) (fa x)))]
      [`(,(? symbol? s) #\^ ,a ..1 'x)
       (define fa (flst->f a))
       (define op (hash-ref ops s))
                                    
       (lambda (x) (expt (op x) (fa x)))]
      [`(,(? symbol? s) #\^ ,a ..1 (,b ..1))
       (define fa (flst->f a))
       (define fb (flst->f b))
       (define op (hash-ref ops s))

       (lambda (x) (expt (op (fb x)) (fb a)))]
      [`(,(? symbol? s) (,a ..1))
       (define fa (flst->f a))
       (define op (hash-ref ops s))

       (lambda (x) (op (fa x)))]
      [`(,(? symbol? s) x)
       (define op (hash-ref ops s))

       (lambda (x) (op x))]
      [(or (? number? n) `(,(? number? n))) n]
      [(or `(x) 'x) (lambda (x) x)]
      [(or `(,s) s) (hash-ref consts s)]
      [`((,a ...)) (flst->f a)]))

  (flst->f (fchars->flst (string->list fstr))))